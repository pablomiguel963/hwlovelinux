# La placa mare (motherboard)

La **placa mare**, **placa base** o **targeta mare** (en [anglès](https://ca.wikipedia.org/wiki/Anglès) **motherboard**)[[1\]](https://ca.wikipedia.org/wiki/Placa_mare#cite_note-Engadget-1) és la [targeta de circuits impresos](https://ca.wikipedia.org/wiki/Circuit_imprès) central a l'[ordinador](https://ca.wikipedia.org/wiki/Ordinador) que conté el [microprocessador](https://ca.wikipedia.org/wiki/Microprocessador), la memòria [RAM](https://ca.wikipedia.org/wiki/Memòria_d'accés_aleatori) del sistema, circuits electrònics de suport, la [ROM](https://ca.wikipedia.org/wiki/Memòria_ROM) i [ranures especials](https://ca.wikipedia.org/wiki/Ranura_d'expansió) (*slots*) que permeten la connexió de targetes adaptadores addicionals.

** Falta explicar qué és un bus

Un altre aspecte important d'una placa base és el "form factor" o factor de forma. Hi ha uns estàndards que marquen la mida i el lloc dels forats per fixar la placa mare a la capsa.  Els més importants són:

- ATX (tower): 305 × 244 mm
- micro-ATX (mini-tower): 244 × 244 mm
- EATX (Servidors): 305 × 272 mm
- mini-ITX (capses molt petites): 170 × 170 mm
- Altres no estàndar per servidors



## Principals fabricants de plaques mare

Si volem comprar un ordinador per parts podem comprar una placa mare dels següents fabricants:

- [Asrock](https://www.asrock.com/) 
- [Asus](https://www.asus.com)
- [Gigabyte](https://www.gigabyte.com/)
- [MSI](https://www.msi.com/)
- [Intel (servidors)](https://www.intel.com/content/www/us/en/products/servers/server-boards.html)

- [Supermicro (servidors)](https://www.supermicro.com/)

A les webs de tots aquets fabricants hem de buscar pel producte "motherboards" i és habitual que estiguin clasificades per socket o chipset. 



### 1. Factor de forma

Tamany de la placa mare. Hi han 3 mides de refència:

- **ATX** (305×244)=> dimensió, on estàn els forats per, on estàn les tarjes d'expansió (format *tower* de caixa)

- microATX (244×244) => hi ha menys espai per tarjes d'expansió (*mini tower* )

- mini-ITX => per models molt petits (per exemple caixes d'ordinador per multimedia)

- Extended ATX => per servidors o plaques més grans

- Altres per servidors en rack

  ![](../img/mainboard_1603783062.png)

## 2. Parts de la placa mare

### 2. 1 Socket i processador

Un conjunt de processadors d'una mateixa arquitectura i generació comparteixen un encapsulat compatible. Per exemple un processador intel [i5-7500](https://ark.intel.com/content/www/us/en/ark/products/97123/intel-core-i5-7500-processor-6m-cache-up-to-3-80-ghz.html) i un intel [i7-7700](https://ark.intel.com/content/www/us/en/ark/products/97128/intel-core-i7-7700-processor-8m-cache-up-to-4-20-ghz.html) pertanyen a la 7a generació de processadors d'Intel, amb un encapsulat igual que es correspon amb un socket FCLGA1151. A una mateixa placa base amb aquest tipus de socket li podem "punxar" qualsevol d'aquests dos processadors. A continuació una imatge d'un i5, per la cara superior (on anirà el dissipador), per la cara inferior (la que connecta cada pin amb el socket de la placa mare) i el socket:

![](../img/i5-7500_front.png) ![i5 cpu rear](../img/i5-7500_pins.png)

![](../img/socket_1151.png)

L'elecció del socket és molt important a una placa mare, ja que condiciona la família de processadors compatibles.

#### Sockets habituals a oct-2020

Intel 1151 (rev1, rev2, rev3): des de 2015 fins 2020 diferents arquitectures de processadors Intel

Intel 2066 (Skylake-X): Procedssadors "gama alta" i9, i7... 

Socket AM4: AMD Ryzen 3,5,7,9

AMD TR4: 

AMD sTRX4: AMD Ryzen™ Threadripper™ (alta gama AMD)


** Falten AMD

#### CPUs compatibles

El fabricant és habitual que tingui una llista de cpus compatibles i si la placa és molt moderna **cal fer un update de la bios** (canviar el firmware) per tal d'admetre aquestes cpus.



Per exemple per una placa mare [Gigabyte B450M DS3H](https://www.gigabyte.com/Motherboard/B450M-DS3H-rev-10/support#support-cpu) tenim un llistat de cpus compatibles


![](../img/mainboard_1603783695.png)




**Falta comentar el tema de la refrigeració

### 2.2 Chipset i DMI

Governa els dispositius de menys velocitat. La CPU governa directament unes lanes de PCI Express per connectar la tarja gràfica o dispositius PCI i la memòria. Altres dispositius es poden connectar a lanes PCI que governa el chipset  o fent servir altres tipus de bus.

Els chipset tenen la capacitat de connectar un nombre màxim de dispositius. És el fabricant el que decideix quants d'aquests dispositius seràn accesibles per l'usuari. 

Per exemple, pel chipset z490 de intel tenim aquestes [especificacions](https://www.intel.es/content/www/es/es/products/chipsets/desktop-chipsets/z490.html) que ens dona el fabricant del chipset que és Intel. Tenim 6 ports SATA que pot governar el chipset.

![](../img/mainboard_1603785663.png)

I tenim per dos plaques mare ( [Z490 AORUS XTREME WATERFORCE](https://www.gigabyte.com/Motherboard/Z490-AORUS-XTREME-WATERFORCE-rev-1x) i [Z490I AORUS ULTRA](https://www.gigabyte.com/Motherboard/Z490I-AORUS-ULTRA-rev-1x)  ) amb el mateix chipset amb diferentes nombres de ports (6x sata i 4xsata):

![](../img/mainboard_1603785516.png)

### DMI (Direct media interface)

DMI és un estàndard de Intel per comunicar la CPU amb el chipset

**DMI 2.0**: 2GB/s

**DMI 3.0**: 3.93 GB/s

### 2.3 Memòria

Hi ha un llistat de memòries compatibles. Pot haver més d'un llistat si la placa mare és compatible amb més d'una família de cpus. Per exemple per una placa amb socket AMD: B550 AORUS PRO AX a la web de suport tenim dos llistats per AMD Matisse o AMD Renoir

![](../img/mainboard_1603784476.png)

Els llistats són semblants a:

![](../img/mainboard_1603784608.png)

### 2.4 PCI Express

PCIe (PCI Express) protocol que permet connectar targetes d'expansió i comunicar aquestes targetes amb la CPU.

El bus PCIe està estructurat com enllaços [punt a punt](https://ca.wikipedia.org/wiki/Punt_a_punt), *full duplex*,
treballant en sèrie. Cada slot d'expansió porta **1**, 2, **4, 8**, **16** o 32 
enllaços de dades entre la placa base i la targeta connectada.

Els processadors actuals de gama "alta" tenen més lanes de PCI que els processadors de gama més "baixa". 

Predecessors del bus PCIe (molt antics):

- ISA

- PCI
- AGP (gpus)

Hi ha 4 versions del bus PCIe:

- Versió 1.x: 2.5 [GT/s](https://en.wikipedia.org/wiki/GT/s) (hi ha compatibilitat amb aquestes versions més velles)
  - x1: 250 MB/s
  - x16: 4 GB/s
- Versió 2.x: 5 GT/s (hi ha compatibilitat amb aquestes versions més velles)
  - **x1:** 500 MB/s
  - **x16:** 8 GB/s
- **Versió 3.x: 8 GT/s** (és el més habitual)
  - **x1: 985 MB/s**
  - **x16: 15.75 GB/s**
- **Version 4.0:16 GT/s** (de moment a oct-2020 disponible amb processadors AMD)
  - **x1: 1.97 GB/s**
  - **x16: 31.5 GB/s**

Tipus de dispositius que es connecten al bus PCIe:

- **Targetes gràfiques** (poden estar integrades al processador o tarjetes dedicades). És el dispositiu més exigent a nivell de velocitat, per això fem servir una amplada de bus de **16 lanes**

- **Targetes de xarxa**. L'amplada del bus que cal per a cada targeta depèn de la velocitat de xarxa que hem de manegar:

  - Per 10Gbps: 10/8 = 1.25 GBps per cada lane de pcie v3 tinc **x1: 985 MB/s**

    - Com que he de suportar fluxos de dades de transmissió i recepció en realitat necessito 2 * 1,25 GBps = **2,5GBps** i necessito 4 lanes PCIe v3 ó 8 lanes PCIe v2
    - x1: 985 MBps = 0.96 GBps
    - x2: 1970 MBps = 1.92 GBps
    - **x4: 3940 MBps = 3.84 GBps**

    ![](../img/mainboard_1603786436.png)

  - Per 1Gbps = 125MBps i tinc suficient amb una lane x1 PCIe v3:

    ![](../img/mainboard_1603787350.png)

- **Targeta de so**: el audio consumeix molt poc ample de banda i és habitual que funcionin amb 1 lane PCI

  ![](../img/mainboard_1603787909.png)

- Unitats **NVME** (enmagatzematge molt ràpid). Per cada dispositui NVME necessito 4 lanes de PCIe. Tenen habitualment un connector diferent que es el **M.2** que envia 4 lanes PCIe.  La velocitat màxima de un disc nvme amb 4 lanes PCIe v3 és de 3940 MBps ( **3.84 GBps**)

- ![](../img/mainboard_1603788378.png)

  - Per exemple per un disc nvme ***Samsung 970 EVO Plus 500GB SSD NVMe M.2*** tenim:

    - Velocidad de lectura: 3500 MB/s

    - Velocidad de escritura: 3200 MB/s

  - Hi han tarjetes que permeten fer una conversió de connectors de PCIe a M.2

    ![](../img/mainboard_1603788491.png)

- Controladores de disc per connectar molts discos ssd o rotacionals en raid



 DMI or direct, gpus, dimensió tarjetes, high low profile

### 2.5 Emmagatzematge





#### Bus IDE / SCSI

Ja no es fan servir, però encara quedan ordinadors vells que en tenen. Les dades es transmeten en paralel i no permet velocitats elevades.

![](../img/mainboard_1603884760.png)



#### Bus SATA (Serial ATA)

Bus serie, amb menys cables transmet més velocitat de dades. Tamany reduit que permet integrar a la placa mare molts connectors.

![](../img/mainboard_1603885046.png)

Amb la tecnologia SSD (Solid state disk) es podian aconseguir velocitats molt elevades. Van treure diferents estàndars per aconseguir aquesta velocitat:

- **SATA**: 150 MB/s
- **SATA 2**: 300 MB/s
- **SATA 3**: 600 MB/s

Si el bus SATA és més ràpid pot arribar a saturar el bus DMI i per això van tindre que canviar de tecnologia del BUS

Les conversions de bus SATA a PCI generen latència.

El connector SATA tradicional és:

![](../img/mainboard_1603885046.png)

M.2 per SATA que es diferencia del M.2 NVME per la forma:

![](../img/mainboard_1603887854.png)

### NVME

Tecnologia de bus que permet velocitats molt elevades i integrar-se amb un bus PCIe de 4 lanes

PCIe v3: **x4: 3940 MBps = 3.84 GBps**

PCIe v4: **x4: 7880 MBps = 7.69 GBps**



Exemple: amb un disc Kinston A2000 tenim aquestes especificacions:

- Velocidad de lectura: 2200 MB/s

- Velocidad de escritura: 2000 MB/s



**Connectors:**

- PCIe (els primers discos nvme eren tarjetes PCI)
- M.2 (és el que es fa servir més). Te diferents formats compatibles en funció del tamany
- U.2 (no es fa servir)

**Tamany**:

hi han diferents estàndars, el més habitual és el M.2 2280

![](../img/mainboard_1603887934.png)

Un chipset integra una controladora que pot governar un nombre de ports.



## 2.7 Audio

Chips o controladors d'audio que es comunican amb el chipset

Els connectors d'audio:

- Rear panel 

  ![](../img/mainboard_1603888851.png)

- Front pannel

  ![](../img/mainboard_1603888766.png)

## 2.8 USB

USB (universal serial bus) pensat per connectar qualsevol tipus de dispositiu

Connectors:

![](../img/mainboard_1603889021.png)



Versions:

- USB 1.1
- USB 2 
- USB 3.0
- USB 3.1
- USB 3.2 : 20 Gbit/s
- USB 4: 40 Gbps

## Port Serie (COM)

Segueix els estàndars de port serie més antics. Es pot connectar externament amb un adapatador. Si hi han diferents ports serie es numeren  com COM1, COM2...

![](../img/mainboard_1603889720.png)

### Connectors d'alimentació

** Falta explicar connectors power

### Connectors frontals

### Ventiladors

FAN (2 o 4 pines) per connectar ventiladors

* CPU_FAN (4 pines) => pel ventilador de la CPU
* CHASIS_FAN, SYSFAN => per la capsa
* FAN1, FAN2

### ResetBIOS





## 3. Comandes de linux per extreure informació de la placa mare

## 3.1 dmidecode

Les taules DMI de la placa base contenen informació sobre versions, models de bios... 

Puc fer servir la comanda dmidecode. 

```
[root@localhost ~]# dmidecode -t baseboard
# dmidecode 3.2
Getting SMBIOS data from sysfs.
SMBIOS 2.7 present.

Handle 0x0002, DMI type 2, 15 bytes
Base Board Information
	Manufacturer: Gigabyte Technology Co., Ltd.
	Product Name: H61M-USB3H
```

L'ordinador del professor te la placa mare de Gigabyte model H61M-USB3H

L'ordinador de l'alumne te la placa mare de Gigabyte model H81M-S2PV

Puc anar a internet i buscar el manual, a un buscardor: 

  site:gigabyte.com H81M-S2PV 

I el manual pdf el tenim a la url https://download.gigabyte.com/FileList/Manual/mb_manual_ga-h81m-s2pv_e.pdf

## Informació sobre la memòòria

```
[root@localhost ~]# dmidecode -t memory
# dmidecode 3.2
Getting SMBIOS data from sysfs.
SMBIOS 2.7 present.

Handle 0x0007, DMI type 16, 23 bytes
Physical Memory Array
	Location: System Board Or Motherboard
	Use: System Memory
	Error Correction Type: None
	Maximum Capacity: 32 GB
	Error Information Handle: Not Provided
	Number Of Devices: 2

Handle 0x0040, DMI type 17, 34 bytes
Memory Device
	Array Handle: 0x0007
	Error Information Handle: Not Provided
	Total Width: Unknown
	Data Width: Unknown
	Size: No Module Installed
	Form Factor: DIMM
	Set: None
	Locator: ChannelB-DIMM0
	Bank Locator: BANK 2
	Type: Unknown
	Type Detail: None
	Speed: Unknown
	Manufacturer: [Empty]
	Serial Number: [Empty]
	Asset Tag: 9876543210
	Part Number: [Empty]
	Rank: Unknown
	Configured Memory Speed: Unknown

Handle 0x0041, DMI type 17, 34 bytes
Memory Device
	Array Handle: 0x0007
	Error Information Handle: Not Provided
	Total Width: 64 bits
	Data Width: 64 bits
	Size: 8 GB
	Form Factor: DIMM
	Set: None
	Locator: ChannelA-DIMM0
	Bank Locator: BANK 0
	Type: DDR3
	Type Detail: Synchronous
	Speed: 1333 MT/s
	Manufacturer: Kingston
	Serial Number: 9A3AE49A
	Asset Tag: 9876543210
	Part Number: 99U5471-040.A00LF 
	Rank: 2
	Configured Memory Speed: 1333 MT/s

```



## 3.2 lspci

ls => list pci=> bus pci   : llistar informació del bus pci

Podem connectar fins a 256 dispositius

```

root@tux:~# lspci --help
lspci: invalid option -- '-'
Usage: lspci [<switches>]

Basic display modes:
-mm		Produce machine-readable output (single -m for an obsolete format)
-t		Show bus tree

Display options:
-v		Be verbose (-vv or -vvv for higher verbosity)
-k		Show kernel drivers handling each device
-x		Show hex-dump of the standard part of the config space
-xxx		Show hex-dump of the whole config space (dangerous; root only)
-xxxx		Show hex-dump of the 4096-byte extended config space (root only)
-b		Bus-centric view (addresses and IRQ's as seen by the bus)
-D		Always show domain numbers
-P		Display bridge path in addition to bus and device number
-PP		Display bus path in addition to bus and device number

Resolving of device ID's to names:
-n		Show numeric ID's
-nn		Show both textual and numeric ID's (names & numbers)
-q		Query the PCI ID database for unknown ID's via DNS
-qq		As above, but re-query locally cached entries
-Q		Query the PCI ID database for all ID's via DNS

Selection of devices:
-s [[[[<domain>]:]<bus>]:][<slot>][.[<func>]]	Show only devices in selected slots
-d [<vendor>]:[<device>][:<class>]		Show only devices with specified ID's

Other options:
-i <file>	Use specified ID database instead of /usr/share/misc/pci.ids.gz
-p <file>	Look up kernel modules in a given file instead of default modules.pcimap
-M		Enable `bus mapping' mode (dangerous; root only)

PCI access options:
-A <method>	Use the specified PCI access method (see `-A help' for a list)
-O <par>=<val>	Set PCI access parameter (see `-O help' for a list)
-G		Enable PCI access debugging
-H <mode>	Use direct hardware access (<mode> = 1 or 2)
-F <file>	Read PCI configuration dump from a given file

```

Opcions destacades:

* -mm		Produce machine-readable output : dona informació de models, tipus de dispusitiu en un format "human"

* -t		Show bus tree: arbre de dispositius
* -v		Be verbose (-vv or -vvv for higher verbosity): dona informació ampliada

* -k		Show kernel drivers handling each device: quins moduls de kernel es fan servir per cada dispositiu
* -nn		Show both textual and numeric ID's (names & numbers)

Selection of devices:

* -s [[[[<domain>]:]<bus>]:][][.[<func>]]	Show only devices in selected slots
* -d [<vendor>]:[<device>][:]		Show only devices with specified ID's

Combinaciones útiles:

* **lspci -vmm**
* **lspci -vmm -nn**
* **lspci -vmm -nn -k**



# 4. Extras

redirigir la sortida d'una comanda a un fitxer: **>**

```
dmesg > /tmp/dmesg_output 
```

Per llegir el fitxer puc fer servir visors (solament per veure) o editors (puc escriure).

* Visors:

  * command line:
    * cat => treu tota la info per pantalla
    * more => comença amb el principi i sol pots avançar endavant
    * **less** => hem permet navegar i permet:
      * **fer cerques** (amb la tecla **/**)
      * propera cerca (n)
      * cerca anterior (N)
    * head -n N => N primeres línies del fitxer
    * tail -n N => N darreres línies del fitxer
  * gui (graphical user interface):
    * lectors de pdf (evince...)
    * meld (per comparar fitxers)

* Editors:

  * command line:
    * vi 
    * **vim** (super potent)
    * emacs (semblant al vi)
    * pico (editor més senzill)
    * nano (editor més senzill)
  * gui: 
    * typora (editar markdowns)
    * geany (editor simple amb sintaxis amb colors)
    * gvim (versió gràfica de vi)
    * gedit (editor per defecte gnome) / kate (kde) / 
    * visual studio code (extensions per markdown, python, git)

* Utilitats que treballen amb text pla:

  * grep: filtratges: Filtra la línea que conté la expressió

    * Exemple per filtrar per la paraula Ethernet

      [root@f32-isard ~]# lspci -k | **grep Ethernet**
      01:00.0 Ethernet controller: Red Hat, Inc. Virtio network device (rev 01)

    * Filtrar sesnse tenir en compte les minúscules / mayúscules: **-i**

      [root@f32-isard ~]# lspci -k | **grep -i network**
      01:00.0 Ethernet controller: Red Hat, Inc. Virtio network device (rev 01)

    *  Extreure les línies anteriors i posteriors a la coincidència:

      -A (After N lines) 

      -B (Before N lines)

      -C (Context N lines)

      [root@f32-isard ~]# lspci -k | grep **-A 2** -i network 
      01:00.0 Ethernet controller: Red Hat, Inc. Virtio network device (rev 01)
      	Subsystem: Red Hat, Inc. Device 1100
      	Kernel driver in use: virtio-pci
      [root@f32-isard ~]# lspci -k | grep **-B 2** -i network 
      	Kernel driver in use: i801_smbus
      	Kernel modules: i2c_i801
      01:00.0 Ethernet controller: Red Hat, Inc. Virtio network device (rev 01)
      [root@f32-isard ~]# lspci -k | grep **-C 2** -i network 
      	Kernel driver in use: i801_smbus
      	Kernel modules: i2c_i801
      01:00.0 Ethernet controller: Red Hat, Inc. Virtio network device (rev 01)
      	Subsystem: Red Hat, Inc. Device 1100
      	Kernel driver in use: virtio-pci

