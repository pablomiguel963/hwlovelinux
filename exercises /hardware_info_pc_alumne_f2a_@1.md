# Extract hardware info from host with linux

## system

#### system model, bios version and bios date, how old is the hardware?
para ver la version de la bios podemos usar el comando dmidecode con la opcion -s y la key bios-version:
```
[ism26952944@a01 ~]$ sudo dmidecode -s bios-version
F5
```
para ver la fecha de la bios podemos usar el comando dmidecode con la opcion -s y la key bios-release-date
```
[ism26952944@a01 ~]$ sudo dmidecode -s bios-release-date
01/17/2014
```
podemos calcular los años del hardware mediante los años de la bios, tiene un total de 5 años 
## mainboard model, link to manual, link to product
podemos saber el modelo de la placa base mediante el comando dmidecode con la opcion -s y la key baseboard-product-name
```
[ism26952944@a01 ~]$ sudo dmidecode -s baseboard-product-name
H81M-S2PV
```
link del manual:
https://download.gigabyte.com/FileList/Manual/mb_manual_ga-h81m-s2pv_e.pdf
link del producto:
http://es.gigabyte.com/products/page/mb/ga-h81m-s2pvrev_10#kf
#### memory banks (free or occupied)
podemos verlo mediante el comando dmidecode con la opcion -t y la key memory
```
[ism26952944@a01 ~]$ sudo dmidecode -t memory
```
#### how many disks and types can be connected
podemos ver los conectores mediante el comando dmidecode con la opcion -t y la key connector 
```
[ism26952944@a01 ~]$ sudo dmidecode -t connector
```
#### chipset, link to 
http://es.gigabyte.com/products/page/mb/ga-h81m-s2pvrev_10#sp
Chipset Intel® H81 Express
## cpu
podemos ver las caracteristcas de la cpu mediante el comando lscpu
```
[ism26952944@a01 ~]$ lscpu
```

#### cpu model, year, cores, threads, cache 
**modelo de la cpu**: intel pentium G3258
**año de la cpu**: es del año 2014
**cores**: 2
**threads**: 1
**cache**: 
L1d cache:                       64 KiB
L1i cache:                       64 KiB
L2 cache:                        512 KiB
L3 cache:                        3 MiB

#### socket 
tiene solo 1

## pci

#### number of pci slots, lanes available
podemos ver el numero de slots del pci mediante el comando dmidecode con la opcion -t y la key slot
```
[ism26952944@a01 ~]$ sudo dmidecode -t slot
```
 
#### devices connected

#### network device, model, kernel module, speed

#### audio device, model, kernel module

#### vga device, model, kernel module

## hard disks

#### /dev/* , model, bus type, bus speed

#### test fio random (IOPS) and sequential (MBps)